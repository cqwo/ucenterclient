package com.cqwo.ucenter.client.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonHelper {

    private static Logger logger = LoggerFactory.getLogger(JsonHelper.class);

    /**
     * 获取字符型参数
     *
     * @param object 对象
     * @param key    key
     * @return string
     */
    public static String getString(JSONObject object, String key) {

        try {
            return object.getString(key);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return "";
    }

    /**
     * 获取实体
     *
     * @param object object
     * @param key    key
     * @param clazz  class
     * @param <T>    类型
     * @return t
     */
    public static <T> T getEntity(JSONObject object, String key, Class<T> clazz) {

        try {

            return JSON.parseObject(getString(object, key), clazz);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        return null;

    }

    /**
     * 获取整形类型参数
     * @param object object
     * @param key key
     * @return int
     */
    public static Integer getInteger(JSONObject object, String key) {
        try {

            return object.getInteger(key);

        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return 0;
    }
}
