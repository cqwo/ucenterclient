package com.cqwo.ucenter.client.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 刷新token
 *
 * @author cqnews
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RefreshTokenInfo implements Serializable {

    private static final long serialVersionUID = 7049454982015350174L;

    /**
     * uid
     */
    @Builder.Default
    private String uid = "";

    /**
     * token
     */
    @Builder.Default
    private String token = "";


    /**
     * token
     */
    @Builder.Default
    private String refreshToken = "";
}
