/*
 *
 *  * Copyright (C) 2017.
 *  * 用于JAVA项目开发
 *  * 重庆青沃科技有限公司 版权所有
 *  * Copyright (C)  2017.  CqingWo Systems Incorporated. All rights reserved.
 *
 */

package com.cqwo.ucenter.client.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by cqnews on 2017/12/25.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginSuccesInfo implements Serializable {

    private static final long serialVersionUID = -5745809786819093283L;
    /**
     * 用户登录成功Uid
     */
    private String uid = "";


    /**
     * 用户登录token
     */
    private String token = "";

    /**
     * 刷新密钥
     */
    private String refreshToken = "";


    /**
     * 用户模型
     */
    private PartUserInfo userInfo;

    /**
     * 用户分组
     */
    private UserRankInfo userRankInfo;


}
