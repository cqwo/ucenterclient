package com.cqwo.ucenter.client.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqwo.ucenter.client.UCenterClient;
import com.cqwo.ucenter.client.domain.LoginSuccesInfo;
import com.cqwo.ucenter.client.domain.OauthInfo;
import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.RefreshTokenInfo;
import com.cqwo.ucenter.client.errors.UCenterCollect;
import com.cqwo.ucenter.client.exception.*;
import com.cqwo.ucenter.client.helper.JsonHelper;
import com.cqwo.ucenter.client.message.UCenterMessageInfo;
import com.cqwo.ucenter.client.pages.PageInfo;
import com.google.common.base.Strings;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * @author cqnews
 */
public abstract class UcenterClientImpl implements UCenterClient {


    public final String HEADER_API_APIKEY = "X-CWMAPI-ApiKey";

    public final String HEADER_API_APISECRET = "X-CWMAPI-ApiSecret";

    public final String HEADER_API_TOKEN = "X-CWMAPI-Token";

    public final String HEADER_API_OPENID = "X-CWMAPI-OpenId";

    public final String HEADER_API_APPID = "X-CWMAPI-AppId";


    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(UcenterClientImpl.class);


    public abstract RestTemplate getRestTemplate();

    /**
     * Post提交数据
     *
     * @param url  url地址
     * @param maps 参数
     * @param clzz 类型
     * @param <T>  类型
     * @return 返回
     * @throws UCenterException
     */
    @Override
    public <T> T restPost(String url, MultiValueMap<String, Object> maps, Class<T> clzz) throws UCenterException {
        return restPost(url, maps, "", clzz);
    }

    @Override
    public <T> T restPost(String url, MultiValueMap<String, Object> maps, String token, Class<T> clzz) throws UCenterFailException {
        try {


            HttpHeaders headers = new HttpHeaders();


            headers.add(HEADER_API_TOKEN, token);
            headers.add(HEADER_API_APIKEY, getApiKey());
            headers.add(HEADER_API_APISECRET, getApiSecret());
            headers.add(HEADER_API_APPID, getAppId());

            //利用容器实现数据封装，发送
            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(maps, headers);

            System.out.println("利用容器实现数据封装:" + JSON.toJSONString(entity));

            ResponseEntity<String> response = getRestTemplate().postForEntity(url, entity, String.class);

            if (response.getStatusCodeValue() != HttpStatus.OK.value()) {
                throw new Exception("网络异常");
            }


            return JSON.parseObject(response.getBody(), clzz);


        } catch (Exception ex) {
            ex.printStackTrace();
            throw new UCenterFailException(UCenterCollect.NETWORK_FAILED, "网络异常");
        }
    }

    /**
     * Get方法提交
     *
     * @param url   url地址
     * @param token token
     * @param clzz  类型
     * @param <T>   泛型
     * @return T
     * @throws UCenterException 用户异常
     */
    @Override
    public <T> T restGet(String url, String token, Class<T> clzz) throws UCenterException {

        try {

            HttpHeaders headers = new HttpHeaders();


            headers.add(HEADER_API_TOKEN, token);
            headers.add(HEADER_API_APIKEY, getApiKey());
            headers.add(HEADER_API_APISECRET, getApiSecret());


            //利用容器实现数据封装，发送
            HttpEntity<Map> entity = new HttpEntity<Map>(headers);

            ResponseEntity<String> response = getRestTemplate().exchange(url, HttpMethod.GET, entity, String.class);

            if (response.getStatusCodeValue() != 200) {
                throw new UCenterFailException(-1, "网络异常");
            }

            return JSON.parseObject(response.getBody(), clzz);
        } catch (Exception ex) {
            logger.warn(ex.getMessage());
            throw new UCenterFailException(-1, "网络异常");
        }


    }

    //region 用户

    /**
     * 用户注册
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     * @throws UCenterException 用户自定义异常
     */
    @Override
    public void onRegister(String account,
                           String password,
                           String nickName,
                           String avatar,
                           Integer gender,
                           Integer regionId) throws UCenterException {
        onRegister(account, password, nickName, avatar, gender, regionId, 2);
    }

    /**
     * 用户注册
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    @Override
    @HystrixCommand(fallbackMethod = "onRegister2")
    public void onRegister(String account,
                           String password,
                           String nickName,
                           String avatar,
                           Integer gender,
                           Integer regionId,
                           Integer expireHours) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("account", account);
        maps.add("password", password);
        maps.add("nickName", nickName);
        maps.add("avatar", avatar);
        maps.add("gender", gender);
        maps.add("regionId", regionId);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/register"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            JSONObject object = JSONObject.parseObject(messageInfo.getContent()); //System.out.println("解析的Object:" + JSON.toJSONString(object));

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 用户注册降级方案
     *
     * @param account  注册账号
     * @param password 密码
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    public void onRegister2(String account,
                            String password,
                            String nickName,
                            String avatar,
                            Integer gender,
                            Integer regionId,
                            Integer expireHours) throws UCenterException {

        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "用户注册降级方案");
    }

    /**
     * 用户登录
     *
     * @param account  账号
     * @param password 密码
     * @throws UCenterException 用户自定义异常
     */
    @Override
    public void onLogin(String account, String password) throws UCenterException {
        onLogin(account, password, true, 2);
    }

    /**
     * 用户登录
     *
     * @param account     账号
     * @param password    密码
     * @param expireHours 超时时间
     * @throws UCenterException 异常
     * @throws UCenterException 异常
     */
    @Override
    @HystrixCommand(fallbackMethod = "onLogin2")
    public void onLogin(String account,
                        String password,
                        boolean isRefresh,
                        Integer expireHours) throws UCenterException {


        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("account", account);
        maps.add("password", password);
        maps.add("isRefresh", isRefresh ? 1 : 0);
        maps.add("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/login"), maps, UCenterMessageInfo.class);
        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }
        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 用户登录降级
     *
     * @param account  账号
     * @param password 密码
     * @throws UCenterException 异常
     */
    public void onLogin2(String account,
                         String password,
                         boolean isRefresh,
                         Integer expireHours) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "用户登录降级");
    }



    /**
     * 用户token登录
     *
     * @param token       token
     * @throws UCenterException          用户异常
     * @throws UcenterTokenErrorExeption 用户token异常
     */
    @Override
    @HystrixCommand(fallbackMethod = "onLogin3")
    public void onLogin(String token) throws UCenterException, UcenterTokenErrorExeption {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("token", token);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("account/tokenlogin"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());


        } else if (UCenterCollect.TOKEN_EXPIRE.equals(messageInfo.getState())) {

            throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

        } else if (UCenterCollect.TOKEN_ERROR.equals(messageInfo.getState())) {

            throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 用户登录降级
     *
     * @param token token
     * @throws UCenterException 用户登录信息
     */
    public void onLogin3(String token) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "用户登录降级");
    }

    /**
     * 开发接口登录
     *
     * @param openId   opendId
     * @param unionId  联合id
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    @Override
    public UCenterMessageInfo onLogin(String server,
                                      String openId,
                                      String unionId,
                                      String nickName,
                                      String avatar,
                                      Integer gender,
                                      Integer regionId) throws UCenterException {
        return onLogin(server, openId, unionId, nickName, avatar, gender, regionId, 2);
    }

    /**
     * 开发接口登录
     *
     * @param openId   opendId
     * @param unionId  联合id
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     */
    @Override
    @HystrixCommand(fallbackMethod = "onLogin4")
    public UCenterMessageInfo onLogin(String server,
                                      String openId,
                                      String unionId,
                                      String nickName,
                                      String avatar,
                                      Integer gender,
                                      Integer regionId,
                                      Integer expireHours) throws UCenterException {


        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("server", server);
        maps.add("openId", openId);
        maps.add("unionId", unionId);
        maps.add("nickName", nickName);
        maps.add("avatar", avatar);
        maps.add("gender", gender);
        maps.add("regionId", regionId);
        maps.add("expireHours", expireHours);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("oauth/login"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            LoginSuccesInfo loginInfo = JSONObject.parseObject(messageInfo.getContent(), LoginSuccesInfo.class);

            if (loginInfo == null || Strings.isNullOrEmpty(loginInfo.getToken())) {

                throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
            }

            throw new UCenterLoginSuccessException(loginInfo.getUid(), loginInfo.getToken(), loginInfo.getRefreshToken(), loginInfo.getUserInfo(), loginInfo.getUserRankInfo(), messageInfo.getMessage());

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 开发接口登录
     *
     * @param openId   opendId
     * @param unionId  联合id
     * @param nickName 昵称
     * @param avatar   头像
     * @param gender   性别
     * @param regionId 区域
     * @return 登录信息
     * @throws UCenterException Exception
     */
    public UCenterMessageInfo onLogin4(String server,
                                       String openId,
                                       String unionId,
                                       String nickName,
                                       String avatar,
                                       Integer gender,
                                       Integer regionId,
                                       Integer expireHours) throws UCenterException {

        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "用户登录降级");
    }


    /**
     * 更新一条用户信息数据
     *
     * @param uid      更新用户uid
     * @param nickName 更新昵称
     * @param realName 更新真实姓名
     * @param regionId 更新区域Id
     * @param address  更新地址
     * @param bio      更新描述
     * @return 更新信息
     * @throws UCenterException Exception
     */
    @Override
    @HystrixCommand(fallbackMethod = "updatePartUser2")
    public PartUserInfo updateUser(String uid,
                                   String nickName,
                                   String realName,
                                   int regionId,
                                   String address,
                                   String bio) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);
        maps.add("nickName", nickName);
        maps.add("realName", realName);
        maps.add("regionId", regionId);
        maps.add("address", address);
        maps.add("bio", bio);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/update"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {


            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);

        }
        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 更新降级处理方案
     *
     * @param uid      更新用户uid
     * @param nickName 更新昵称
     * @param realName 更新真实姓名
     * @param regionId 更新区域Id
     * @param address  更新地址
     * @param bio      更新描述
     * @return 更新信息
     * @throws UCenterException Exception
     */
    public PartUserInfo updatePartUser2(String uid,
                                        String nickName,
                                        String realName,
                                        int regionId,
                                        String address,
                                        String bio) throws UCenterException {

        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "更新降级处理方案");
    }


    /**
     * 修改用户密码
     *
     * @param mobile      手机号
     * @param newPassword 新密码
     * @return
     * @throws UCenterException
     */
    @Override
    @HystrixCommand(fallbackMethod = "updatePassword2")
    public PartUserInfo updatePassword(String mobile,
                                       String newPassword) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("mobile", mobile);
        maps.add("newPassword", newPassword);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/updatepassword"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }
        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 更新降级处理方案
     *
     * @param uid         用户uid
     * @param mobile      手机号
     * @param newPassword 新密码
     * @return
     * @throws UCenterException
     */
    public PartUserInfo updatePassword2(String uid,
                                        String mobile,
                                        String newPassword) throws UCenterException {

        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "更新降级处理方案");
    }


    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    @Override
    public RefreshTokenInfo refreshToken(String uid, String refreshToken) throws UCenterException {
        return refreshToken(uid, refreshToken, 2);
    }

    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    @Override
    @HystrixCommand(fallbackMethod = "refreshToken2")
    public RefreshTokenInfo refreshToken(String uid, String refreshToken, Integer expireHours) throws UCenterException {


        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);
        maps.add("refreshToken", refreshToken);
        maps.add("expireHours", expireHours);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("token/refresh"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), RefreshTokenInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 刷新token
     *
     * @param uid          uid
     * @param refreshToken 刷新token
     * @return RefreshTokenInfo
     */
    public RefreshTokenInfo refreshToken2(String uid, String refreshToken, Integer expireHours) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "刷新token降级处理方案");
    }


    /**
     * 通过用户uid获取用户信息
     *
     * @param uid uid
     * @return 用户信息
     * @throws UCenterException 异常
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByUid2")
    public PartUserInfo getPartUserByUid(String uid) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyuid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过用户uid获取用户信息降级方案
     *
     * @param uid uid
     * @return 用户信息
     * @throws UCenterException 异常
     */
    public PartUserInfo getPartUserByUid2(String uid) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");

    }


    /**
     * 通过手机号码获取用户
     *
     * @return 用户信息
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByMobile2")
    public PartUserInfo getPartUserByMobile(String mobile) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("mobile", mobile);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbymobile"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过手机号码获取用户
     *
     * @return 用户信息
     */
    public PartUserInfo getPartUserByMobile2(String mobile) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }

    /**
     * 通过用户名获取用户
     *
     * @param userName 用户名
     * @return 用户信息
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByUserName2")
    public PartUserInfo getPartUserByUserName(String userName) throws UCenterException {


        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("userName", userName);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyusername"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 通过用户名获取用户(降级方案)
     *
     * @param userName 用户名
     * @return 用户信息
     */
    public PartUserInfo getPartUserByUserName2(String userName) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }

    /**
     * 通过邮箱获取用户信息
     *
     * @param email 邮箱
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByEmail2")
    public PartUserInfo getPartUserByEmail(String email) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("email", email);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyusername"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过邮箱获取用户信息
     *
     * @param email 邮箱
     */
    public PartUserInfo getPartUserByEmail2(String email) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }

    /**
     * 通过token获取用户信息
     *
     * @param openId openid
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByOpenId2")
    public PartUserInfo getPartUserByOpenId(String openId) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("openId", openId);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyopenid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过token获取用户信息降级方案
     *
     * @param openId openid
     */
    public PartUserInfo getPartUserByOpenId2(String openId) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }

    /**
     * 通过unionId获取用户信息
     *
     * @param unionId unionId
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserByUnionId2")
    public PartUserInfo getPartUserByUnionId(String unionId) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("unionId", unionId);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/findbyunionid"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {
            return JSON.parseObject(messageInfo.getContent(), PartUserInfo.class);
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 通过unionId获取用户信息降级方案
     *
     * @param unionId unionId
     */
    public PartUserInfo getPartUserByUnionId2(String unionId) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }


    /**
     * 获得用户数据列表
     *
     * @param uid      uid
     * @param nickName 昵称
     * @param mobile   手机号
     * @return 返回UserInfo
     **/
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserList2")
    public List<PartUserInfo> getPartUserList(String uid, String nickName, String mobile) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);
        maps.add("nickName", nickName);
        maps.add("mobile", mobile);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            JSONObject object = JSONObject.parseObject(messageInfo.getContent());

            return JSONArray.parseArray(JsonHelper.getString(object, "userInfoList"), PartUserInfo.class);

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());

    }

    /**
     * 获得用户数据列表
     *
     * @param uid      uid
     * @param nickName 昵称
     * @param mobile   手机号
     * @return 返回UserInfo
     **/
    public List<PartUserInfo> getPartUserList2(String uid, String nickName, String mobile) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }


    /**
     * 获取用户列表
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @param uid        uid
     * @param nickName   昵称
     * @param mobile     手机
     * @return 用户列表信息
     * @throws UCenterException 异常
     */
    @Override
    @HystrixCommand(fallbackMethod = "getPartUserList3")
    public void getPartUserList(Integer pageSize, Integer pageNumber, String uid, String nickName, String mobile) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("pageSize", pageSize.toString());
        maps.add("pageNumber", pageNumber.toString());
        maps.add("uid", uid);
        maps.add("nickName", nickName);
        maps.add("mobile", mobile);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            JSONObject object = JSONObject.parseObject(messageInfo.getContent());

            List<PartUserInfo> userInfoList = JSONArray.parseArray(JsonHelper.getString(object, "userInfoList"), PartUserInfo.class);
            Integer totalCount = JsonHelper.getInteger(object, "totalCount");

            PageInfo<PartUserInfo> pageInfo = new PageInfo<>(pageSize, pageNumber, totalCount, userInfoList);

            throw new UCenterPageListSuccessException(pageInfo, messageInfo.getMessage());
        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }


    /**
     * 获取用户列表降级方案
     *
     * @param pageSize   每页条数
     * @param pageNumber 当前页数
     * @param uid        uid
     * @param nickName   昵称
     * @param mobile     手机
     * @return 用户列表信息
     * @throws UCenterException 异常
     */
    public void getPartUserList3(Integer pageSize, Integer pageNumber, String uid, String nickName, String mobile) throws UCenterException {
        throw new UCenterFailException(UCenterCollect.DOWNGRADE, "降级方案");
    }

    /**
     * 判断是否有重复的code
     *
     * @param invitCode 邀请码
     * @return
     * @throws IOException
     */
    public boolean isExitsInvitCode(String invitCode) throws UCenterException {
        return false;
    }

    /**
     * 更新用户组
     *
     * @param uid     用户uid
     * @param userRid 用户分组
     */
    @Override
    public void updateUserRankByUid(String uid, Integer userRid) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);
        maps.add("userRid", userRid);


        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/list"), maps, UCenterMessageInfo.class);
    }

    /**
     * 更新手机
     *
     * @param uid    uid
     * @param mobile 手机
     */
    @Override
    public void updateUserMobile(String uid, String mobile) throws UCenterException {
        //userRepository.updateUserMobileByUid(uid, mobile);

    }

    /**
     * 更新用户金额
     *
     * @param uid   用户uid
     * @param money 金额
     */
    @Override
    public void updateUserMoneyByUid(String uid, double money) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);
        maps.add("money", money);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("user/updatemoney"), maps, UCenterMessageInfo.class);
    }


    /**
     * 获取用户头像
     *
     * @param uid uid
     */
    @Override
    @HystrixCommand(fallbackMethod = "getUserAvatar2")
    public String getUserAvatar(String uid) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);

        return restPost(getApiUrl("user/avatar"), maps, String.class);
    }

    /**
     * 获取用户头像
     *
     * @param uid uid
     */
    public String getUserAvatar2(String uid) throws UCenterException {
        return "http://www.510link.com/img/logo.png";
    }


    /**
     * 通过appid和uid查询第三方登录信息
     *
     * @param uid uid
     */
    @Override
    @HystrixCommand(fallbackMethod = "findOauthByUid2")
    public OauthInfo findOauthByUid(String uid) throws UCenterException {

        MultiValueMap<String, Object> maps = new LinkedMultiValueMap<>();
        maps.add("uid", uid);

        UCenterMessageInfo messageInfo = restPost(getApiUrl("oauth/find"), maps, UCenterMessageInfo.class);

        if (messageInfo == null) {
            throw new UCenterResolveFailedException("数据解析失败");
        }

        if (UCenterCollect.SUCCESS.equals(messageInfo.getState())) {

            return JSON.parseObject(messageInfo.getContent(), OauthInfo.class);

        }

        throw new UCenterFailException(messageInfo.getState(), messageInfo.getMessage());
    }

    /**
     * 通过appid和uid查询第三方登录信息降级方案
     *
     * @param appId appId
     * @param uid   uid
     */
    public String findOauthByUid2(String appId, String uid) throws UCenterException {
        return UCenterMessageInfo.of(-2, "降级方案").toJson();
    }


    //endregion


}
