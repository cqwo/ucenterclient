package com.cqwo.ucenter.client.exception;

import com.cqwo.ucenter.client.domain.PartUserInfo;
import com.cqwo.ucenter.client.domain.UserRankInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author cqnews
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UCenterLoginSuccessException extends UCenterException {

    private static final long serialVersionUID = -7414619674616011144L;

    /**
     * uid
     */
    private String uid = "";

    /**
     * token
     */
    private String token = "";

    /**
     * 刷新token
     */
    private String refreshToken = "";


    /**
     * 用户信息
     */
    private PartUserInfo userInfo;

    /**
     * 用户等级
     */
    private UserRankInfo userRankInfo;

    public UCenterLoginSuccessException(String uid, String token, String refreshToken, PartUserInfo userInfo, UserRankInfo userRankInfo) {
        super("用户登录成功");
        this.uid = uid;
        this.token = token;
        this.refreshToken = refreshToken;
        this.userInfo = userInfo;
        this.userRankInfo = userRankInfo;
    }

    public UCenterLoginSuccessException(String uid, String token, String refreshToken, PartUserInfo userInfo, UserRankInfo userRankInfo, String message) {
        super(message);
        this.uid = uid;
        this.token = token;
        this.refreshToken = refreshToken;
        this.userInfo = userInfo;
        this.userRankInfo = userRankInfo;
    }

}
