package com.cqwo.ucenter.client.exception;

/**
 * token错误
 *
 * @author cqnews
 */
public class UcenterTokenExpireExeption extends UCenterException {

    private static final long serialVersionUID = -4461695275816804854L;

    public UcenterTokenExpireExeption() {
        super();
    }

    public UcenterTokenExpireExeption(String message) {
        super(message);
    }

    public UcenterTokenExpireExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public UcenterTokenExpireExeption(Throwable cause) {
        super(cause);
    }

    protected UcenterTokenExpireExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
