package com.cqwo.ucenter.client.exception;

/**
 * token过期
 *
 * @author cqnews
 */
public class UcenterTokenErrorExeption extends UCenterException {

    private static final long serialVersionUID = -4461695275816804854L;

    public UcenterTokenErrorExeption() {
        super();
    }

    public UcenterTokenErrorExeption(String message) {
        super(message);
    }

    public UcenterTokenErrorExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public UcenterTokenErrorExeption(Throwable cause) {
        super(cause);
    }

    protected UcenterTokenErrorExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
