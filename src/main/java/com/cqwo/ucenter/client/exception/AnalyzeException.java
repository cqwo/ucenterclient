package com.cqwo.ucenter.client.exception;

/**
 * @author cqnews
 */
public class AnalyzeException extends UCenterException {

    private static final long serialVersionUID = 3940722642209755831L;

    public AnalyzeException(String message) {
        super(message);
    }

}
