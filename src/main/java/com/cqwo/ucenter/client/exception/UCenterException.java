package com.cqwo.ucenter.client.exception;

/**
 * @author cqnews
 */
public class UCenterException extends Exception {

    private static final long serialVersionUID = -225399980217669568L;

    public UCenterException() {
        super();
    }

    public UCenterException(String message) {
        super(message);
    }

    public UCenterException(String message, Throwable cause) {
        super(message, cause);
    }

    public UCenterException(Throwable cause) {
        super(cause);
    }

    protected UCenterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
