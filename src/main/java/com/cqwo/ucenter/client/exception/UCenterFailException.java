package com.cqwo.ucenter.client.exception;

/**
 * 用户数据读取失败
 *
 * @author cqnews
 */
public class UCenterFailException extends UCenterException {


    private static final long serialVersionUID = -8990752720070644187L;
    private Integer state;

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public UCenterFailException(Integer state, String message) {
        super(message);
        this.state = state;
    }
}
