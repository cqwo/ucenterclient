package com.cqwo.ucenter.client.exception;

/**
 * token异常
 * @author cqnews
 */
public class UcenterTokenExeption extends UCenterException {

    private static final long serialVersionUID = -4461695275816804854L;

    public UcenterTokenExeption() {
        super();
    }

    public UcenterTokenExeption(String message) {
        super(message);
    }

    public UcenterTokenExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public UcenterTokenExeption(Throwable cause) {
        super(cause);
    }

    protected UcenterTokenExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
