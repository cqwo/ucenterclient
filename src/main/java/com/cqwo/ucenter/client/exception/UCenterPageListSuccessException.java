package com.cqwo.ucenter.client.exception;

import com.cqwo.ucenter.client.pages.PageInfo;

import java.util.List;

/**
 * @author cqnews
 */
public class UCenterPageListSuccessException extends UCenterException {

    private static final long serialVersionUID = 4804962688518329162L;

    private PageInfo pageInfo;

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public UCenterPageListSuccessException(PageInfo pageInfo) {
        super();
        this.pageInfo = pageInfo;
    }

    public UCenterPageListSuccessException(PageInfo pageInfo, String message) {
        super(message);
        this.pageInfo = pageInfo;
    }
}
